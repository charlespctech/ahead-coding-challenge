<# 
Coding Challenge 
•	Write a Python or PowerShell script that accepts a file hash value (MD5 or SHA256) from the user and submits the hash to VirusTotal via an API call.
•	If VirusTotal finds more than 5 AV engines detected the file as malicious, output a message that informs the user and tells them how many AV engines detected the file.
•	If VirusTotal finds that less than 5 AV engines reported the file as malicious, output a message that indicates the file may be malicious and tells the user how many AV engines detected the file.
•	If no AV engines indicate the file is malicious, output a message that tells the user that the file is clean.
This script should also:
•	Validate user input to verify that the user has entered a valid hash.
•	Require the user to enter their API information (required to make API calls to VirusTotal).
•	Return an error message if an invalid hash is entered by the user or if they do not input their API information.
•	Output the API call's status code (200, 404, etc.)
•	Inform the user that the API call failed if status code is not 200. 
#>

#1. Prompt user to insert a MD5 or SHA256 and save as a variable

Write-Host 'Input your MD5 or SHA256 hash' -ForegroundColor Green

$Hash = Read-Host

#2. Validate the input as a valid hash (based on length and asci character types) 

while ($Hash -cnotmatch '^[a-zA-Z0-9]+$' -or ($Hash.length -ne "32" -and "64"))
    
    {
        
        Write-Host 'Not a valid hash. Please re-input your MD5 or SHA256 hash' -ForegroundColor Red

        $Hash = Read-Host

    }      

#3. Ask for the users virustotal API login information

Write-Host 'Provide Virustotal API Key' -ForegroundColor Green

$Api = Read-Host 
                    
#4. Return an error message if they do not input their API information

while ($Api.Length -eq "0")
    
    {
        
        Write-Host 'Missing API information. Please Provide Virustotal API Key' -ForegroundColor Red

        $Api = Read-Host 

    }    

#5. Access virustotal API and submit the hash

$headers=@{}

$headers.Add("Accept", "application/json")

$headers.Add("x-apikey", $Api)

#6. Save API call output as a variable

$response = Invoke-WebRequest -Uri ("https://www.virustotal.com/api/v3/files/" + $Hash) -Method GET -Headers $headers -UseBasicParsing

$response_ps = $response | ConvertFrom-Json
            
#7. extract and save number of detections as variable
            
$count=$response_ps.data.attributes.last_analysis_stats.malicious

#8. Output the API call's status code (200, 404, etc.)

Write-Host $response.statuscode -ForegroundColor Green

#9. Check if status code was 200. Inform the user that the API call failed if status code is not 200.

if ($response.StatusCode -ne "200")
        
    {
        
        Write-Host 'The API call failed.' -ForegroundColor Red
        
    }
                
#10. If VirusTotal finds more than 5 AV engines detected the file as malicious, output a message that informs the user and tells them how many AV engines detected the file.

if ($count -ge "5")
        
    {
        
        Write-Host 'File is Malicious. '$count 'AV engines detected the file as malicious.' -ForegroundColor Red
        
    }

#11. If VirusTotal finds that less than 5 AV engines reported the file as malicious, output a message that indicates the file may be malicious and tells the user how many AV engines detected the file.
            
if ($count -lt "5")
       
    {
        
        Write-Host "File may be Malicious." $count 'AV engines detected the file as malicious.' -ForegroundColor DarkYellow
        
    }

#12. If no AV engines indicate the file is malicious, output a message that tells the user that the file is clean.
            
if ($count -eq "0")
        
    {
    
        Write-Host 'File is clean.' -ForegroundColor Green
    
    }
